![Kenzie Academy graduation](https://pbs.twimg.com/media/Dz35nZMWsAEhsmL.jpg)

Welcome to our free introductory class. We're so excited to have you take this first step with us. This course will introduce you to web programming, including brief lessons in:
1. HTML
2. CSS
3. JavaScript (coming soon)

More than learning a set of technologies, we hope you'll learn that coding is fun and something you'll be successful at. And finally, we hope you agree that Kenzie Academy will be a great place to start your new career.

Let's get started. 